<?php

namespace Opyn\OpynPayLater\Observer;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Opyn\OpynPayLater\Helper\OpynHelper;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Store\Model\StoreManagerInterface;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Customer\Model\Session as CustomerSession;

/**
 *  This Class is needed to conditional rendering the payment method
 */
class PaymentMethodAvailable implements ObserverInterface
{
    /** @var CheckoutSession */
    protected $checkoutSession;
    /** @var OpynHelper */
    protected $opynHelper;
    /** @var StoreManagerInterface */
    protected $storeManager;

    /**
     * @param CheckoutSession $checkoutSession
     * @param OpynHelper $opynHelper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        OpynHelper $opynHelper,
        StoreManagerInterface $storeManager
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->opynHelper = $opynHelper;
        $this->storeManager = $storeManager;
    }

    /**
     * Event handler.
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $store = $this->storeManager->getStore();
        $session=$this->checkoutSession->getQuote()->getShippingAddress();
        /* if currency is not EUR or opyn checkOrder return false */
        if ($observer->getEvent()->getMethodInstance()->getCode() == "opyn" &&
            (
                $store->getWebsite()->getBaseCurrency()->getCurrencyCode() != 'EUR'
                ||
                !$this->opynHelper->checkOrder(
                    $session->getVatId() ?? '',
                    $session->getCompany() ?? '',
                    $session->getCountry() ?? '',
                    $session->getPostcode() ?? '',
                    $this->checkoutSession->getQuote()->getData('base_grand_total')
                )
            )
        ) {
            $checkResult = $observer->getEvent()->getResult();
            $checkResult->setData('is_available', false); //this is disabling the payment method at checkout page
        }
    }
}
