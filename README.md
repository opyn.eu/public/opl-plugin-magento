# opyn-magento-plugin

## Quick install 

Run the following commands in order.

* `mkdir magento.test`
* `cd magento.test`
* `curl -s https://raw.githubusercontent.com/markshust/docker-magento/master/lib/onelinesetup | bash -s -- magento.test 2.4.6 community`
* `bin/magento sampledata:deploy`
* `bin/magento setup:upgrade`
* `bin/magento module:disable Magento_AdminAdobeImsTwoFactorAuth`
* `bin/magento module:disable Magento_TwoFactorAuth`
* `php bin/magento setup:upgrade`
* `php bin/magento setup:static-content:deploy -f`

## Full install specifications
### Automatic Magento2 Setup

based on https://github.com/markshust/docker-magento
x
Run the following command before cloning this extension, the repo will create all for you

```bash
# Run this automated one-liner from the directory you want to install your project.
curl -s https://raw.githubusercontent.com/markshust/docker-magento/master/lib/onelinesetup | bash -s -- magento.test 2.4.6 community
```

The `magento.test` above defines the hostname to use, and the `2.4.6` defines the Magento version to install. Note that since we need a write to `/etc/hosts` for DNS resolution, you will be prompted for your system password during setup.

After the one-liner above completes running, you should be able to access your site at `https://magento.test`.

#### Install sample data

After the above installation is complete, run the following command, from php container, to install sample data:

```bash
bin/magento sampledata:deploy
bin/magento setup:upgrade
```

### Manual Setup

Same result as the one-liner above. Just replace `magento.test` references with the hostname that you wish to use.

all magento2 filer will be into src/ folder

### Remove MFA
By default magento2 hs MFA enabled.
with this commands
```bash
bin/magento module:disable Magento_AdminAdobeImsTwoFactorAuth
bin/magento module:disable Magento_TwoFactorAuth 
```

### URL
`magento.test/admin` to enter to backend

Credentials
- USER: john.smith
- PASSWORD: password123


### Module Setup
To install the module put the entire folder `Opyn` into `./src/app/code/` and then run the following commands from the php container

```bash
bin/magento module:enable Opyn_OpynPayLater
bin/magento setup:upgrade
bin/magento setup:static-content:deploy -f
```

### Opyn Pay Later
Configure this module in backend configuration in stores>configuration>payment methods section and enable it.

After that you need to go in currency settings (stores>configuration>general>currency setup) and set the currencies in Euro, otherwise the OPL button will not be shown


