/**
 * @api
 */
define([
    'jquery',
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/abstract'
], function ($, _, registry, AbstractField) {
    'use strict';

    return AbstractField.extend({
        defaults: {
            skipValidation: false
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            $('#co-shipping-form [name="shippingAddress.vat_id"]').show();
            $('#co-shipping-form [name="vat_id"]').show();
        }
    });
});
