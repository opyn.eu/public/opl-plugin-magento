    define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'opyn',
                component: 'Opyn_OpynPayLater/js/view/payment/method-renderer/opynpay-method'
            }
        );
        return Component.extend({});
    }
);
