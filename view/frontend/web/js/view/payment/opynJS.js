require(['jquery'], function($){
    var data;
    var OpynAccessToken='';
    var OpynMageOrderId ='';
    var OpynMageOrderTotal ='';
    var isApproval = false;
    var opynCreateOrderUrl = '';
    var counterConditions = 0;
    // defaul locale, but will be overridden in flow
    var magentoLocale = 'en_US';
    $( document ).ready(function() {
        function validateVat(){
            if( document.querySelector('#co-shipping-form [name="vat_id"]')) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    async: false,
                    // contentType: "application/json",
                    url: window.location.origin + "/opyn_validatevat/index/ValidateVat",
                    data: {
                        validateVat: document.querySelector('#co-shipping-form [name="vat_id"]').value,
                        country: document.querySelector('#co-shipping-form [name="country_id"]').value,
                        company: document.querySelector('#co-shipping-form [name="company"]').value,
                        postcode: document.querySelector('#co-shipping-form [name="postcode"]').value,
                    },
                    success: function (data) {
                    },
                });
            }
        }

        $('body').on('change keyup','input[name="vat_id"]',function(){
            validateVat();
        });
        $('body').on('change keyup','input[name="company"]',function(){
            validateVat();
        });
        $('body').on('change keyup','input[name="country_id"]',function(){
            validateVat();
        });
        $('body').on('change keyup','input[name="postcode"]',function(){
            validateVat();
        });
        validateVat();

        //wait until the last element (.payment-method) being rendered
        var existCondition = setInterval(function() {
            if ($('.payment-method #opyn').length) {
                clearInterval(existCondition);
                opynPayLater();
            }
            // limit loops
            counterConditions ++;
            if( counterConditions > 1000 ){
                clearInterval(existCondition);
            }
        }, 100);

        function opynPayLater(){
            var tokenUrl=window.location.origin+'/opyn_login/index/OpynToken';
            var customerEmail=$('#customer-email').val();
            var mageCacheStorageJson=this.window.localStorage['mage-cache-storage'];

            // if opyn disabled must be enabled, then script not will download with block and JS managment checkout
            if(
                typeof opyn == "object" &&
                typeof opyn.payLater == "function"
            ){
                opyn.payLater({
                    createOrder: async() => {
                        isApproval = false;
                        //Get Opyn Token
                        $.ajax({
                            type: "GET",
                            url: tokenUrl,
                            async: false,
                            success: function (login) {

                                resStart=login.search("access_token");
                                response=login.slice(resStart-2);
                                responseJSON= JSON.parse(response)

                                if (!responseJSON.access_token) {
                                    alert('Authentication error');
                                }

                                OpynAccessToken = responseJSON.access_token;
                            },
                        });

                        // Create MAGENTO Order (as draft)
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            async: false,
                            // contentType: "application/json",
                            url: window.location.origin+"/opyn_createorder/index/CreateOrder",
                            data: {
                                'email': customerEmail,
                                'data': mageCacheStorageJson
                            },
                            success: function (MageOrder) {
                                OpynMageOrderId=MageOrder.order_id;
                                OpynMageOrderTotal=MageOrder.total;
                                opynCreateOrderUrl=MageOrder.opyn_create_order_url;
                                magentoLocale=MageOrder.magentoLocale;
                            },
                            error: function (){
                                //window.location.reload()
                            }
                        });

                        // Create OPYN Order
                        var headers = new Headers();
                        headers.append('Content-Type', 'application/json');
                        headers.append('Authorization', 'Bearer ' + OpynAccessToken);

                        return await fetch(opynCreateOrderUrl, {
                            method: 'POST',
                            headers,
                            body: JSON.stringify({
                                taxcode: document.querySelector('#co-shipping-form [name="vat_id"]').value,
                                amount: parseFloat(OpynMageOrderTotal),
                                notes: "",
                                country: document.querySelector('#co-shipping-form [name="country_id"]').value,
                                companyName: document.querySelector('#co-shipping-form [name="company"]').value,
                                postalCode: document.querySelector('#co-shipping-form [name="postcode"]').value,
                                locale: magentoLocale
                            })
                        });
                    },
                    onApproval: (orderId) => {
                        isApproval = true;
                        // check the order status
                        data = new FormData();
                        data.append( 'orderId', OpynMageOrderId );

                        //Confirm MAGE Order
                        $.ajax({
                            method: 'POST',
                            type: 'POST',
                            dataType: "json",
                            url: window.location.origin+"/opyn_completeorder/index/CompleteOrder",
                            data: {
                                'orderId': OpynMageOrderId
                            },
                            success: function (MageOrder) {
                                require('Magento_Customer/js/customer-data').reload(['cart']);
                            },
                        });

                    },
                    onReject: (message) => {
                        data = new FormData();
                        data.append( 'orderId', OpynMageOrderId );
                        data.append( 'customerEmail', $('#customer-email').val() );

                        // Cancel MAGENTO Order
                        $.ajax({
                            contentType: false,
                            processData: false,
                            method: 'POST',
                            type: 'POST',
                            url: window.location.origin+"/opyn_cancelorder/index/CancelOrder",
                            data: data,
                            success: function (data) {
                            },
                        });
                        opyn.reload();
                    },
                    onClose: () => {
                        //payment window has been closed
                        if( !isApproval ){
                            data = new FormData();
                            data.append( 'orderId', OpynMageOrderId );
                            data.append( 'customerEmail', $('#customer-email').val() );

                            // Cancel MAGENTO Order
                            $.ajax({
                                contentType: false,
                                processData: false,
                                method: 'POST',
                                type: 'POST',
                                url: window.location.origin+"/opyn_cancelorder/index/CancelOrder",
                                data: data,
                                success: function (data) {
                                },
                            });
                        }else{
                            // if is approval finish order and go to thank you page
                            window.location.href = window.location.origin+'/opyn_finishorder/index/FinishOrder?orderId='+OpynMageOrderId;
                        }
                    }
                });
            }
            else{
                location.reload();
            }
        }
    });
});
