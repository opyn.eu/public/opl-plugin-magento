<?php
namespace Opyn\OpynPayLater\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Renders the check credential via button
 */
class CheckCredentials extends Field
{
    /**
     * Preparing Layout
     *
     * @return $this|CheckCredentials
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate('system/config/fieldset/check_credential_button.phtml');
        }

        return $this;
    }

    /**
     * Render as html
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }
}
