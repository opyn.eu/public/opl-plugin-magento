<?php

namespace Opyn\OpynPayLater\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Context;
use Magento\Framework\Module\PackageInfoFactory;
use Magento\Config\Block\System\Config\Form\Field\Heading as Heading;

class ModuleInfo extends Heading
{
    /**
     * @var PackageInfoFactory
     */
    protected $packageInfoFactory;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PackageInfoFactory $packageInfoFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        PackageInfoFactory $packageInfoFactory,
        array $data = []
    ) {
        $this->packageInfoFactory = $packageInfoFactory;
        parent::__construct($context, $data);
    }

    /**
     * Render element html
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $label = $element->getLabel();

        $info = "<style>
            #row_payment_us_opyn_prod_js,
            #row_payment_us_opyn_sandbox_js,
            #row_payment_us_opyn_sandbox_login,
            #row_payment_us_opyn_prod_login,
            #row_payment_us_opyn_sandbox_create_order,
            #row_payment_us_opyn_prod_create_order,
            #row_payment_us_opyn_prod_check_order,
            #row_payment_us_opyn_sandbox_check_order
            {display:none;}
        </style>";
        $label .= $info;

        return sprintf(
            '<tr class="system-fieldset-sub-head" id="row_%s"><td colspan="5"><h4 id="%s">%s</h4></td></tr>',
            $element->getHtmlId(),
            $element->getHtmlId(),
            $label
        );
    }
}
