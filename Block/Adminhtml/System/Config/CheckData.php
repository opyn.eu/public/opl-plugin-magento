<?php

namespace Opyn\OpynPayLater\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Context;
use Magento\Framework\Module\PackageInfoFactory;
use Magento\Config\Block\System\Config\Form\Field\Heading as Heading;
use Magento\Framework\Data\Form\Element\AbstractElement;

class CheckData extends Heading
{
    /**
     * @var PackageInfoFactory
     */
    protected $packageInfoFactory;

    /**
     * @param Context $context
     * @param PackageInfoFactory $packageInfoFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        PackageInfoFactory $packageInfoFactory,
        array $data = []
    ) {
        $this->packageInfoFactory = $packageInfoFactory;
        parent::__construct($context, $data);
    }

    /**
     * Render element html
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $label = '';
        $info = '<button type="button">Verifica credenziali</button>';
        $label .= $info;

        return sprintf(
            '<tr class="system-fieldset-sub-head" id="row_%s"><td colspan="5"><h4 id="%s">%s</h4></td></tr>',
            $element->getHtmlId(),
            $element->getHtmlId(),
            $label
        );
    }
}
