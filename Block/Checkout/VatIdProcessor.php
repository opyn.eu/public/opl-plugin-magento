<?php

namespace Opyn\OpynPayLater\Block\Checkout;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Block\Checkout\LayoutProcessor;

/**
 * Manage, check and add as required the vat field
 */
class VatIdProcessor
{
    /** @const string */
    private const CONFIG_PATH_REQUIRED_COUNTRIES = 'customer/address/vat_required_countries';
    /** @const string */
    private const CONFIF_PATH_HIDE_VAT_FIELD = 'customer/address/hide_vat_field_not_required';
    /**  @var StoreManagerInterface */
    private StoreManagerInterface $_storeManager;
    /** @var ScopeConfigInterface */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Checkout LayoutProcessor after process plugin.
     *
     * @param LayoutProcessor $processor
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(LayoutProcessor $processor, $jsLayout)
    {

        $shippingConfiguration = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];

        //Checks if shipping step available.
        if (isset($shippingConfiguration)) {
            $shippingConfiguration = $this->processAddress(
                $shippingConfiguration,
                'shippingAddress',
                [
                    'checkoutProvider',
                    'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.country_id'
                ]
            );
        }

        return $jsLayout;
    }

    /**
     * Process provided address to contains checkbox and have trackable address fields.
     *
     * @param array $addressFieldset Address fieldset config.
     * @param string $dataScope data scope
     * @param array $deps list of dependencies
     * @return array
     */
    private function processAddress(array $addressFieldset, string $dataScope, array $deps): array
    {

        $addressFieldset['vat_id'] = [
            'component' => 'Opyn_OpynPayLater/js/vatid-dependency',
            'label' => __('VAT number'),
            'config' => [
                'customScope' => $dataScope,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'requiredCountries' => $this->getConfigValue(self::CONFIG_PATH_REQUIRED_COUNTRIES),
                'hideVatFieldIfNotRequired' => $this->getConfigValue(self::CONFIF_PATH_HIDE_VAT_FIELD),
                'tooltip' => [
                    'description' => __('Enter your VAT number if you have one.'),
                    'short' => __('VAT number'),
                ],
            ],
            'deps' => $deps,
            'provider' => 'checkoutProvider',
            'dataScope' => 'shippingAddress.vat_id',
            'sortOrder' => 140,
            'validation' => [
                'required-entry' => true
            ]
        ];

        return $addressFieldset;
    }

    /**
     * Returns field
     *
     * @param string $field
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getConfigValue(string $field)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getStore()->getId()
        );
    }
}
