<?php

declare(strict_types=1);

namespace Opyn\OpynPayLater\Block;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Opyn\OpynPayLater\Model\Config\Settings;

/**
 * This block is used to render the script include
 */
class Script extends Template
{

    /** @var Settings*/
    protected Settings $settings;
    /** @var CheckoutSession */
    protected CheckoutSession $checkoutSession;

    /**
     * @param Context $context
     * @param Settings $settings
     * @param CheckoutSession $checkoutSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        Settings $settings,
        CheckoutSession $checkoutSession,
        array $data = []
    ) {
        $this->settings = $settings;
        $this->checkoutSession = $checkoutSession;

        parent::__construct($context, $data);
    }

    /**
     * Get the Script source, based on env
     *
     * @return string
     */
    public function getSource(): string
    {
        $session=$this->checkoutSession;

        $opynEnabled = $session->getOpynEnabled();
        if (!($opynEnabled ?? false)) {
            return '';
        }

        $opynSettings=$this->settings->getExtParameters();
        $clientId = $opynSettings['client_id'];
        $js = $opynSettings[ $opynSettings['environment'].'_js'].'?client_id=';

        return $js.$clientId;
    }
}
