<?php
declare(strict_types=1);

namespace Opyn\OpynPayLater\Controller\Index;

use Magento\Checkout\Model\Cart;
use Magento\Framework\Locale\Resolver;
use Opyn\OpynPayLater\Helper\OpynHelper;
use Opyn\OpynPayLater\Model\Config\Settings;
use Opyn\OpynPayLater\Helper\OrderHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\HTTP\Adapter\Curl;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteManagement;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 *  This Controller is needed to check opyn order
 */
class CheckOrder implements HttpPostActionInterface
{
    /** @var Quote */
    protected Quote $quote;
    /** @var QuoteManagement */
    protected QuoteManagement $quoteManagement;
    /** @var StoreManagerInterface */
    protected StoreManagerInterface $storeManager;
    /** @var Context */
    protected Context $context;
    /** @var CustomerRepositoryInterface */
    protected CustomerRepositoryInterface $customerRepository;
    /** @var JsonFactory */
    protected JsonFactory $resultJsonFactory;
    /** @var Http */
    protected Http $request;
    /** @var Curl */
    protected Curl $curl;
    /** @var OrderHelper */
    protected OrderHelper $orderHelper;
    /** @var Resolver*/
    protected Resolver $localeResolver;
    /** @var Cart*/
    protected Cart $cart;

    /**
     * @param Context $context
     * @param Quote $quote
     * @param QuoteManagement $quoteManagement
     * @param StoreManagerInterface $storeManager
     * @param Http $request
     * @param CustomerRepositoryInterface $customerRepository
     * @param JsonFactory $resultJsonFactory
     * @param Curl $curl
     * @param OrderHelper $orderHelper
     * @param ProductRepositoryInterface $productRepository
     * @param Settings $settings
     * @param OpynHelper $opynHelper
     * @param Resolver $localeResolver
     * @param Cart $cart
     */
    public function __construct(
        Context $context,
        Quote $quote,
        QuoteManagement $quoteManagement,
        StoreManagerInterface $storeManager,
        Http $request,
        CustomerRepositoryInterface $customerRepository,
        JsonFactory $resultJsonFactory,
        Curl $curl,
        OrderHelper $orderHelper,
        ProductRepositoryInterface $productRepository,
        Settings $settings,
        OpynHelper $opynHelper,
        Resolver $localeResolver,
        Cart $cart
    ) {
        $this->productRepository = $productRepository;
        $this->context = $context;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->customerRepository=$customerRepository;
        $this->resultJsonFactory=$resultJsonFactory;
        $this->curl=$curl;
        $this->orderHelper=$orderHelper;
        $this->settings=$settings;
        $this->opynHelper=$opynHelper;
        $this->cart=$cart;
    }

    /**
     * Execute
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException */
    public function execute()
    {
        $vatId=$this->request->get('taxcode');
        $company=$this->request->get('company');
        $country=$this->request->get('country');
        $postcode=$this->request->get('postcode');
        $billingEmail=$this->request->get('email');
        $mageCacheStorageJson=$this->request->get('data');
        $cart = $this->cart;
        $productsArray= [];
        $mageCacheStorage=json_decode($mageCacheStorageJson);
        $address=$mageCacheStorage->{'checkout-data'}->shippingAddressFromData;
        $shippingMethod=$mageCacheStorage->{'checkout-data'}->selectedShippingRate;
        $store = $this->storeManager->getStore();
        $items = $cart->getQuote()->getAllVisibleItems();

        foreach ($items as $p => $product) {
            $productsArray[]=[
                'product_id' => $product['product_id'],
                'sku'=> $product['sku'],
                'qty' => $product['qty'],
                'price'=>$product['price']
            ];
        }

        $orderData = [
            'currency' => $store->getWebsite()->getBaseCurrency()->getCurrencyCode(),
            'email' => $billingEmail,
            'shipping_address' => [
                'firstname' => $address->firstname,
                'lastname' => $address->lastname,
                'street' => $address->street->{'0'},
                'city' => $address->city,
                'country_id' => $address->country_id,
                'region' => $address->region,
                'region_id' => $address->region_id,
                'postcode' => $address->postcode,
                'telephone' => $address->telephone,
                'save_in_address_book' => 0
            ],
            'items' => $productsArray,
        ];

        $result=$this->orderHelper->createMageOrder($orderData, $shippingMethod, false);

        // opyn checkOrder
        if ($vatId == null) {
            $vat_id = '';
        }
        if ($company == null) {
            $company = '';
        }
        if ($country == null) {
            $country = '';
        }
        if ($postcode == null) {
            $postcode = '';
        }
        $checkOrder = $this->opynHelper->checkOrder($vatId, $company, $country, $postcode, $result['total']);
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData(['vatId'=>$vatId,'total'=>$result['total'],'checkOrder'=>$checkOrder]);
    }
}
