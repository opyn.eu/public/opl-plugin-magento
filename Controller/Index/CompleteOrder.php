<?php
declare(strict_types=1);

namespace Opyn\OpynPayLater\Controller\Index;

use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;

/**
 *  This Controller is needed to complete Magento order
 */
class CompleteOrder implements HttpPostActionInterface
{
    /** @var OrderInterface */
    protected $orderInterface;
    /** @var Context */
    protected $context;
    /** @var Http */
    protected Http $request;
    /** @var JsonFactory */
    protected JsonFactory $resultJsonFactory;
    /** @var OrderSender */
    protected $orderSender;
    /** @var Cart*/
    protected Cart $cart;

    /** @param Context $context
     * @param OrderInterface $orderInterface
     * @param Http $request
     * @param JsonFactory $resultJsonFactory
     * @param OrderSender $orderSender
     * @param Cart $cart
     */
    public function __construct(
        Context $context,
        OrderInterface $orderInterface,
        Http $request,
        JsonFactory $resultJsonFactory,
        OrderSender $orderSender,
        Cart $cart
    ) {
        $this->context = $context;
        $this->orderInterface = $orderInterface;
        $this->request=$request;
        $this->resultJsonFactory=$resultJsonFactory;
        $this->orderSender=$orderSender;
        $this->cart=$cart;
    }

    /**
     * Execute
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception */
    public function execute()
    {
        $orderId=$this->request->get('orderId');
        $order = $this->orderInterface->loadByIncrementId($orderId);
        $order->setStatus('complete');
        $this->orderSender->send($order, true);
        $order->save();

        if ($order->getEntityId()) {
            $result =[
                'error' => 0,
                'order_id'=>$order->getRealOrderId(),
                "status"=> $order->getStatus(),
                'message'=>'Order successfully completed'
            ];
        } else {
            $result =[
                'error' => 1,
                'order_id'=>'',
                'message'=> 'ERROR'
            ];
        }

        $resultJson = $this->resultJsonFactory->create();
        $this->cart->truncate()->save();
        return $resultJson->setData($result);
    }
}
