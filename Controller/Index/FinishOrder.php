<?php
declare(strict_types=1);

namespace Opyn\OpynPayLater\Controller\Index;

use Magento\Framework\DataObject;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Checkout\Model\Session;

/**
 *  This Controller is needed to Finish, so check and redirect
 *  (after completion) the Magento order
 */
class FinishOrder extends Action
{
    /** @var OrderInterface */
    protected OrderInterface $orderInterface;
    /** @var Context */
    protected Context $context;
    /** @var Http */
    protected Http $request;
    /** @var JsonFactory */
    protected JsonFactory $resultJsonFactory;
    /** @var Session */
    protected Session $checkoutSession;

    /**
     * @param Context $context
     * @param OrderInterface $orderInterface
     * @param Http $request
     * @param JsonFactory $resultJsonFactory
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        OrderInterface $orderInterface,
        Http $request,
        JsonFactory $resultJsonFactory,
        Session $checkoutSession
    ) {
        parent::__construct($context);

        $this->context = $context;
        $this->orderInterface = $orderInterface;
        $this->request=$request;
        $this->resultJsonFactory=$resultJsonFactory;
        $this->checkoutSession=$checkoutSession;
    }

    /**
     * Execute
     *
     * @return void
     */
    public function execute()
    {
        $result = new DataObject();

        $orderId=$this->request->get('orderId');
        $order = $this->orderInterface->loadByIncrementId($orderId);

        $this->checkoutSession->setLastSuccessQuoteId($order->getQuoteId());
        $this->checkoutSession->setLastQuoteId($order->getQuoteId());
        $this->checkoutSession->setLastOrderId($orderId);

        $result->setData('success', true);
        $result->setData('error', false);

        $this->_eventManager->dispatch(
            'checkout_controller_onepage_saveOrder',
            [
                'result' => $result,
                'action' => $this
            ]
        );
        $this->_redirect('checkout/onepage/success');
    }
}
