<?php
declare(strict_types=1);

namespace Opyn\OpynPayLater\Controller\Index;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 *  This Controller is needed to verify VAT for opyn order
 */
class ValidateVat implements HttpPostActionInterface
{
    /** @var Context */
    protected $context;
    /** @var Http */
    protected Http $request;
    /** @var JsonFactory */
    protected JsonFactory $resultJsonFactory;
    /** @var CheckoutSession */
    protected CheckoutSession $checkoutSession;

    /**
     * @param Context $context
     * @param Http $request
     * @param JsonFactory $resultJsonFactory
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Context $context,
        Http $request,
        JsonFactory $resultJsonFactory,
        CheckoutSession $checkoutSession
    ) {
        $this->context = $context;
        $this->request = $request;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Execute
     *
     * @return \Magento\Framework\App\ResponseInterface|Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $vat = $this->request->get('validateVat');
        $country = $this->request->get('country');
        $company = $this->request->get('company');
        $postcode = $this->request->get('postcode');
        $this->checkoutSession->setValidateVat($vat);
        $this->checkoutSession->setCountry($country);
        $this->checkoutSession->setCompany($company);
        $this->checkoutSession->setPostcode($postcode);
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData('');
    }
}
