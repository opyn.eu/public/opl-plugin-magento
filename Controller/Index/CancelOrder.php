<?php
declare(strict_types=1);

namespace Opyn\OpynPayLater\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Api\Data\OrderInterface;

/**
 *  This Controller is needed to Cance Magento Order
 *  in cse OPY reject the order
 */
class CancelOrder implements HttpPostActionInterface
{
    /** @var OrderInterface */
    protected OrderInterface $orderInterface;
    /** @var Context */
    protected Context $context;
    /** @var Http */
    protected Http $request;
    /** @var JsonFactory */
    protected JsonFactory $resultJsonFactory;

    /**
     * @param Context $context
     * @param OrderInterface $orderInterface
     * @param Http $request
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        OrderInterface $orderInterface,
        Http $request,
        JsonFactory $resultJsonFactory
    ) {
        $this->context = $context;
        $this->orderInterface = $orderInterface;
        $this->request = $request;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * Execute
     *
     * @return \Magento\Framework\App\ResponseInterface|Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $orderId=$this->request->get('orderId');
        $customerEmail=$this->request->get('customerEmail');
        $order = $this->orderInterface->loadByIncrementId($orderId);

        if ($order->getId() && $order->getCustomerEmail() == $customerEmail) {
            // Flag the order as 'cancelled' and save it
            $order->setStatus('canceled');
            $order->save();

            $result =[
                'error' => 0,
                'order_id'=>$order->getRealOrderId(),
                'message'=> 'Order successfully canceled'
            ];
        } else {
            $result =[
                'error' => 1,
                'order_id'=>'',
                'message'=> 'ERROR'
            ];
        }

        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData($result);
    }
}
