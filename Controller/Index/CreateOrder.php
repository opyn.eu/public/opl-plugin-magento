<?php
declare(strict_types=1);

namespace Opyn\OpynPayLater\Controller\Index;

use Opyn\OpynPayLater\Helper\OrderHelper;
use Opyn\OpynPayLater\Model\Config\Settings;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteManagement;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Locale\Resolver;
use Magento\Checkout\Model\Cart;

/**
 *  This Controller is needed to create magento order
 */
class CreateOrder implements HttpPostActionInterface
{
    /** @var Quote $quote */
    protected Quote $quote;
    /** @var QuoteManagement*/
    protected QuoteManagement $quoteManagement;
    /** @var StoreManagerInterface*/
    protected StoreManagerInterface $storeManager;
    /** @var Context*/
    protected Context $context;
    /** @var CustomerRepositoryInterface*/
    protected CustomerRepositoryInterface $customerRepository;
    /** @var JsonFactory*/
    protected JsonFactory $resultJsonFactory;
    /** @var Http*/
    protected Http $request;
    /** @var Curl*/
    protected Curl $curl;
    /** @var OrderHelper*/
    protected OrderHelper $orderHelper;
    /** @var Settings*/
    protected Settings $settings;
    /** @var Resolver*/
    protected Resolver $localeResolver;
    /** @var Cart*/
    protected Cart $cart;
    /** @var ProductRepositoryInterface*/
    protected ProductRepositoryInterface $productRepository;

    /** @param Context $context
     * @param Quote $quote
     * @param QuoteManagement $quoteManagement
     * @param StoreManagerInterface $storeManager
     * @param Http $request
     * @param CustomerRepositoryInterface $customerRepository
     * @param JsonFactory $resultJsonFactory
     * @param Curl $curl
     * @param OrderHelper $orderHelper
     * @param ProductRepositoryInterface $productRepository
     * @param Settings $settings
     * @param Cart $cart
     * @param Resolver $localeResolver*/
    public function __construct(
        Context $context,
        Quote $quote,
        QuoteManagement $quoteManagement,
        StoreManagerInterface $storeManager,
        Http $request,
        CustomerRepositoryInterface $customerRepository,
        JsonFactory $resultJsonFactory,
        Curl $curl,
        OrderHelper $orderHelper,
        ProductRepositoryInterface $productRepository,
        Settings $settings,
        Cart $cart,
        Resolver $localeResolver
    ) {
        $this->productRepository = $productRepository;
        $this->context = $context;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->customerRepository = $customerRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->curl = $curl;
        $this->orderHelper = $orderHelper;
        $this->settings = $settings;
        $this->cart = $cart;
        $this->localeResolver = $localeResolver;
    }

    /**
     * Execute
     *
     * @return \Magento\Framework\Controller\Result\Json
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException*/
    public function execute()
    {
        $opyn_settings = $this->settings->getExtParameters();
        $environment = $opyn_settings['environment'] ?? 'sandbox';
        $cart = $this->cart;
        $productsArray = [];
        $billingEmail = $this->request->get('email');
        $mageCacheStorageJson = $this->request->get('data');
        $mageCacheStorage = json_decode($mageCacheStorageJson);
        $address = $mageCacheStorage->{'checkout-data'}->shippingAddressFromData;
        $shippingMethod = $mageCacheStorage->{'checkout-data'}->selectedShippingRate;
        $store = $this->storeManager->getStore();
        $magentoLocale = $this->localeResolver->getLocale();
        $opynCreateOrderUrl = $opyn_settings[$environment . '_create_order'] ?? '';

        $items = $cart->getQuote()->getAllVisibleItems();

        foreach ($items as $p => $product) {
            $productsArray[] = [
                'product_id' => $product['product_id'],
                'sku' => $product['sku'],
                'qty' => $product['qty'],
                'price' => $product['price']
            ];
        }

        $orderData = [
            'currency' => $store->getWebsite()->getBaseCurrency()->getCurrencyCode(),
            'email' => $billingEmail,
            'shipping_address' => [
                'firstname' => $address->firstname,
                'lastname' => $address->lastname,
                'street' => $address->street->{'0'},
                'city' => $address->city,
                'country_id' => $address->country_id,
                'region' => $address->region,
                'region_id' => $address->region_id??1,//1 in case not provided
                'postcode' => $address->postcode,
                'telephone' => $address->telephone,
                'save_in_address_book' => 0
            ],
            'items' => $productsArray,
        ];

        $result = $this->orderHelper->createMageOrder($orderData, $shippingMethod);
        $result['opyn_create_order_url'] = $opynCreateOrderUrl;
        $result['magentoLocale'] = $magentoLocale;

        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($result);
    }
}
