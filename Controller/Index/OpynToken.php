<?php
declare(strict_types=1);

namespace Opyn\OpynPayLater\Controller\Index;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Opyn\OpynPayLater\Helper\CommonHelper;
use Opyn\OpynPayLater\Model\Config\Settings;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\HTTP\Adapter\Curl;
use Magento\Framework\App\Request\Http;

/**
 *  This Controller is needed to retrieve Token from OPYN
 */
class OpynToken implements HttpGetActionInterface
{

    /** @var Settings $settings */
    protected Settings $settings;

    /** @var CommonHelper $commonHelper */
    protected CommonHelper $commonHelper;

    /** @var JsonFactory $resultJsonFactory */
    protected JsonFactory $resultJsonFactory;

    /** @var Curl $curl */
    protected Curl $curl;

    /** @var Http $request */
    protected Http $request;

    /**
     * Construct
     *
     * @param JsonFactory $resultJsonFactory
     * @param CommonHelper $commonHelper
     * @param Settings $settings
     * @param Curl $curl
     * @param Http $request
     */
    public function __construct(
        JsonFactory $resultJsonFactory,
        CommonHelper $commonHelper,
        Settings $settings,
        Curl $curl,
        Http $request
    ) {
        $this->settings=$settings;
        $this->resultJsonFactory=$resultJsonFactory;
        $this->commonHelper=$commonHelper;
        $this->curl=$curl;
        $this->request=$request;
    }

    /**
     * Execute
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $opyn_settings=$this->settings->getExtParameters();
        $checkCredentials = $this->request->get('check_credentials');
        $environment = $opyn_settings['environment'] ?? 'sandbox';

        $post_fields=[
            'client_id'=> $opyn_settings['client_id'] ?? '',
            'client_secret'=> $opyn_settings['client_secret'] ?? ''
        ];
        $apiUrl =   $opyn_settings[ $environment .'_login'] ?? '';
        if ($checkCredentials == 1) {
            $environment = $this->request->get('environment');
            $sandboxLogin = $this->request->get('sandbox_login');
            $prodLogin = $this->request->get('prod_login');
            if ($environment == 'prod') {
                $apiUrl = $prodLogin;
            } else {
                $apiUrl = $sandboxLogin;
            }

            $clientId = $this->request->get('client_id');
            $clientSecret =  $this->request->get('client_secret');
            if (str_replace('*', '', $clientId) != '') {
                $post_fields['client_id'] = $clientId;
            }
            if (str_replace('*', '', $clientSecret) != '') {
                $post_fields['client_secret'] = $clientSecret;
            }
        }

        $this->curl->write('POST', $apiUrl, '1.1', ['Content-Type: application/json'], json_encode($post_fields));
        $result = $this->curl->read();

        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setData($result);
        return $resultJson;
    }
}
