<?php
namespace Opyn\OpynPayLater\Helper;

use Magento\Checkout\Model\Session as CheckoutSession;
use Opyn\OpynPayLater\Model\Config\Settings;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\HTTP\Adapter\Curl;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Opyn related functions
 */
class OpynHelper extends AbstractHelper
{
    /** @var Context */
    protected Context $context;
    /** @var Settings */
    protected Settings $settings;
    /** @var Curl */
    protected Curl $curl;
    /** @var Http */
    protected Http $request;
    /** @var JsonFactory $resultJsonFactory */
    protected JsonFactory $resultJsonFactory;
    /** @var CheckoutSession $checkoutSession */
    protected CheckoutSession $checkoutSession;

    /**
     * @param Context $context
     * @param Settings $settings
     * @param Curl $curl
     * @param Http $request
     * @param JsonFactory $resultJsonFactory
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Context $context,
        Settings $settings,
        Curl $curl,
        Http $request,
        JsonFactory $resultJsonFactory,
        CheckoutSession $checkoutSession
    ) {
        $this->settings = $settings;
        $this->curl = $curl;
        $this->request = $request;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    /**
     * Retrieve Opyn Token
     *
     * @return array
     */
    public function token():array
    {
        $opyn_settings=$this->settings->getExtParameters();
        $checkCredentials = $this->request->get('check_credentials');
        $environment = $opyn_settings['environment'] ?? 'sandbox';
        $post_fields=[
            'client_id'=> $opyn_settings['client_id'] ?? '',
            'client_secret'=> $opyn_settings['client_secret'] ?? ''
        ];
        $apiUrl =  $opyn_settings[ $environment .'_login'] ?? '';

        if ($checkCredentials == 1) {
            $environment = $this->request->get('environment');
            $sandboxLogin = $this->request->get('sandbox_login');
            $prodLogin = $this->request->get('prod_login');
            if ($environment == 'prod') {
                $apiUrl = $prodLogin;
            } else {
                $apiUrl = $sandboxLogin;
            }

            $clientId = $this->request->get('client_id');
            $clientSecret =  $this->request->get('client_secret');
            if (str_replace('*', '', $clientId)!='') {
                $post_fields['client_id'] = $clientId;
            }
            if (str_replace('*', '', $clientSecret)!='') {
                $post_fields['client_secret'] = $clientSecret;
            }
        }

        $this->curl->write('POST', $apiUrl, '2.0', ['Content-Type: application/json'], json_encode($post_fields));
        $result = $this->curl->read();
//        $result = explode('{', $result );
//        $result = '{'.$result[1];

        if ($result!='' && preg_match('/\{/', $result)) {
            $result = explode('{', $result);
            $result = '{'.$result[1];
        } else {
            $result='{"access_token":""}';
        }

        return json_decode($result, JSON_OBJECT_AS_ARRAY);
    }

    /**
     * Check Opyn Order
     *
     * @param string $vatId
     * @param string $companyName
     * @param string $country
     * @param string $postalCode
     * @param string $amount
     * @return bool
     */
    public function checkOrder(
        string $vatId,
        string $companyName,
        string $country,
        string $postalCode,
        string $amount
    ):bool {
        $opyn_settings=$this->settings->getExtParameters();
        $environment = $opyn_settings['environment'] ?? 'sandbox';
        $apiUrlSettings = $opyn_settings[ $environment .'_check_order'] ?? '';

        $token = $this->token();

        $apiUrl = $apiUrlSettings .
            '?taxCode='.$vatId.
            '&amount='.$amount.
            "&country=".$country.
            "&companyName=".$companyName.
            "&postalCode=".$postalCode;

        $this->curl->write(
            'GET',
            $apiUrl,
            '1.1',
            [
                'Authorization:Bearer '.$token['access_token']
            ]
        );

        $result = $this->curl->read();

        $return = false;
        if (substr($result, -4) == 'true') {
            $return = true;
        }
        $this->checkoutSession->setOpynEnabled($return);

        return $return;
    }
}
