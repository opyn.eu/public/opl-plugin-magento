<?php
/**
 * Copyright © Opyn, Inc. All rights reserved.
 */
declare(strict_types=1);

namespace Opyn\OpynPayLater\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\App\Helper\Context;

/**
 * Common functions
 */
class CommonHelper extends AbstractHelper
{
    /** @var Curl */
    protected $_curl;
    /** @var Context */
    protected $context;

    /**
     * @param Curl $curl
     * @param Context $context
     */
    public function __construct(
        Curl $curl,
        Context $context
    ) {
        $this->_curl = $curl;
        $this->context = $context;
        parent::__construct($context);
    }

    /**
     * Checks whether it is a built-in function call.
     *
     * @param string $type
     * @param string $url
     * @param string $body
     * @param string $header
     * @return string
     */
    public function curl(string $type, string $url, string $body, string $header): string
    {
        $opt = [
            'CURLOPT_URL' => $url,
            'CURLOPT_RETURNTRANSFER' => true,
            'CURLOPT_ENCODING' => "",
            'CURLOPT_MAXREDIRS' => 0,
            'CURLOPT_TIMEOUT' => 0,
            'CURLOPT_FOLLOWLOCATION' => true,
            'CURLOPT_HTTP_VERSION' => 'URL_HTTP_VERSION_1_1',
            'CURLOPT_SSL_CIPHER_LIST' => 'DEFAULT@SECLEVEL=1',
            'CURLOPT_HTTPHEADER' => $header,
            'CURLOPT_VERBOSE' => true,
            'CURLOPT_SSL_VERIFYPEER' => true
        ];

        $this->_curl->setOptions($opt);

        if ($type=='GET') {
            $this->_curl->get($url);
        } else {
            $this->_curl->post($url, $body);
        }

        $result = $this->_curl->getBody();

        if (!$result) {
            return "Connection Failure";
        }

        return $result;
    }
}
