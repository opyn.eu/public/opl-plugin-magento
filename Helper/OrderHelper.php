<?php
namespace Opyn\OpynPayLater\Helper;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\Product;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteManagement;
use Magento\Customer\Model\CustomerFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Sales\Model\Order;

/**
 * Common Order functions
 */
class OrderHelper extends AbstractHelper
{
    /** @var Context */
    protected $context;
    /** @var StoreManagerInterface */
    protected $_storeManager;
    /** @var Product */
    protected $_product;
    /** @var QuoteFactory */
    protected $quote;
    /** @var QuoteManagement */
    protected $quoteManagement;
    /** @var CustomerFactory */
    protected $customerFactory;
    /** @var CustomerRepositoryInterface $customerRepository */
    protected $customerRepository;
    /** @var CartRepositoryInterface */
    protected $cartRepositoryInterface;
    /** @var CartManagementInterface */
    protected $cartManagementInterface;
    /** @var Order */
    protected $order;
    /** @var Configurable */
    protected $configurable;
    /** @var ProductRepositoryInterface */
    protected $productRepository;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param StoreManagerInterface $storeManager
     * @param Product $product ,
     * @param CartRepositoryInterface $cartRepositoryInterface ,
     * @param CartManagementInterface $cartManagementInterface ,
     * @param CustomerFactory $customerFactory ,
     * @param CustomerRepositoryInterface $customerRepository ,
     * @param Order $order
     * @param Configurable $configurable
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Product $product,
        CartRepositoryInterface $cartRepositoryInterface,
        CartManagementInterface $cartManagementInterface,
        CustomerFactory $customerFactory,
        CustomerRepositoryInterface $customerRepository,
        Order $order,
        Configurable $configurable,
        ProductRepositoryInterface $productRepository,
    ) {
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->order = $order;
        $this->configurable = $configurable;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    /**
     * Create Order On Your Store
     *
     * @param array $orderData
     * @param string|null $shippingMethod
     * @param bool $createOrder
     * @return array|int[]
     * @throws CouldNotSaveException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function createMageOrder(array $orderData, string|null $shippingMethod, bool $createOrder = true):array
    {
        $store=$this->_storeManager->getStore();
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $customer=$this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($orderData['email']);// load customet by email address
        if (!$customer->getEntityId()) {
            //If not avilable then create this customer
            $customer->setWebsiteId($websiteId)
                ->setStore($store)
                ->setFirstname($orderData['shipping_address']['firstname'])
                ->setLastname($orderData['shipping_address']['lastname'])
                ->setEmail($orderData['email'])
                ->setPassword($orderData['email']);
            $customer->save();
        }

        $cartId = $this->cartManagementInterface->createEmptyCart(); //Create empty cart
        $quote = $this->cartRepositoryInterface->get($cartId); // load empty cart quote
        $quote->setStore($store);

        // if you have allready buyer id then you can load customer directly
        $customer= $this->customerRepository->getById($customer->getEntityId());
        $quote->setCurrency();
        $quote->assignCustomer($customer); //Assign quote to customer

        //add items in quote
        foreach ($orderData['items'] as $k => $item) {

            $product = $this->productRepository->get($item['sku']);

            if ($product->getTypeId() == 'configurable') {
                continue;
            }

            $product->setPrice($item['price']);
            $quote->addProduct($product, (int)($item['qty']));
        }

        //Set Address to quote
        $quote->getBillingAddress()->addData($orderData['shipping_address']);
        $quote->getShippingAddress()->addData($orderData['shipping_address']);

        $quote->save(); //Now Save quote and your quote is ready

        // Collect Rates and Set Shipping & Payment Method
        $shippingAddress=$quote->getShippingAddress();
        $shippingAddress->collectShippingRates();

        if ($shippingMethod=='') {
            $allShippment=$shippingAddress->getShippingRatesCollection()->toArray();
            $shippingMethod=$allShippment['items'][0]['code'];
        }
        $shippingAddress->setShippingMethod($shippingMethod); //shipping method
        $quote->setPaymentMethod('opyn'); //payment method
        $quote->setInventoryProcessed(false); //not effetc inventory

        // Set Sales Order Payment
        $quote->getPayment()->importData(['method' => 'opyn']);
        if (!$createOrder) {
            return ['total' => $quote->getGrandTotal()];
        }
        // Collect Totals ---> verificare perche nn mi prende una cippa
        $quote->collectTotals();
        $quote->save(); //Now Save quote and your quote is ready

        // Create Order From Quote
        $quote = $this->cartRepositoryInterface->get($quote->getId());
        $orderId = $this->cartManagementInterface->placeOrder($quote->getId());
        $order = $this->order->load($orderId);
        $order->setEmailSent(0);

        $result=[];
        if ($order->getEntityId()) {
            $result=[
                'order_id' => $order->getRealOrderId(),
                'total' => $order->getGrandTotal(),
                'opyn_create_order_url' => ''
            ];
        } else {
            $result=[
                'error'=>1
            ];
        }
        return $result;
    }
}
