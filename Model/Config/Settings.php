<?php
declare(strict_types=1);

namespace Opyn\OpynPayLater\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;

/**
 *  Extension setting
 **/
class Settings
{
    /** @var ScopeConfigInterface */
    protected $scopeConfig;
    /** @var StoreManagerInterface */
    protected $storeManager;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Retrieve the Current Store Code
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCurrentStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }

    /**
     * Plugins Parameter
     *
     * This contains the settings variables
     *
     * @return array
     */
    public function getExtParameters()
    {
        $scope=ScopeInterface::SCOPE_STORE;
        return [
                'environment'=>$this->scopeConfig->getValue('payment/opyn/environment', $scope),
                'active'=>$this->scopeConfig->getValue('payment/opyn/active', $scope),
                'client_id' =>$this->scopeConfig->getValue('payment/opyn/client_id', $scope),
                'client_secret' =>$this->scopeConfig->getValue('payment/opyn/client_secret', $scope),
                'sandbox_js' => $this->scopeConfig->getValue('payment/opyn/sandbox_js', $scope),
                'prod_js' => $this->scopeConfig->getValue('payment/opyn/prod_js', $scope),
                'sandbox_login' => $this->scopeConfig->getValue('payment/opyn/sandbox_login', $scope),
                'prod_login' => $this->scopeConfig->getValue('payment/opyn/prod_login', $scope),
                'sandbox_create_order' => $this->scopeConfig->getValue('payment/opyn/sandbox_create_order', $scope),
                'prod_create_order' => $this->scopeConfig->getValue('payment/opyn/prod_create_order', $scope),
                'prod_check_order' => $this->scopeConfig->getValue('payment/opyn/prod_check_order', $scope),
                'sandbox_check_order' => $this->scopeConfig->getValue('payment/opyn/sandbox_check_order', $scope),
            ];
    }
}
