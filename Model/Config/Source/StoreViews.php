<?php
declare(strict_types=1);

namespace Opyn\OpynPayLater\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Store view
 */
class StoreViews implements ArrayInterface
{
    /** @var StoreManagerInterface */
    protected $storeManager;

    /**
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * Common
     *
     * @return array
     */
    public function toOptionArray() : array
    {
        $list = [];
        $stores = $this->getAllStores();
        foreach ($stores as $store) {
            $list[] = [
                'value' => $store->getCode(),
                'label' => $store->getName()
            ];
        }

        return $list;
    }

    /**
     * Get store views
     *
     * @return \Magento\Store\Api\Data\StoreInterface[]
     */
    protected function getAllStores()
    {
        return $this->storeManager->getStores();
    }
}
