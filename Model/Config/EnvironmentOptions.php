<?php

namespace Opyn\OpynPayLater\Model\Config;

use \Magento\Framework\Data\OptionSourceInterface;

/**
 * The env source
 */
class EnvironmentOptions implements OptionSourceInterface
{
    /**
     * Setting up Sandbox or prod env
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'value' => 'sandbox',
            'label' => 'Sandbox',
        ];
        $options[] = [
            'value' => 'prod',
            'label' => 'Production',
        ];

        return $options;
    }
}
