<?php
declare(strict_types=1);

namespace Opyn\OpynPayLater\Model\Gateways;

use Opyn\OpynPayLater\Model\Method;

/**
 *
 * Decaling OPYN method
 */
class Opyn extends Method
{
    /** @var string */
    public $_code = 'opyn';
    /** @var string */
    public $_gateWayCode = 'opyn';
}
