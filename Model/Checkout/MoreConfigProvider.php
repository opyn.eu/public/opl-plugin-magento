<?php

namespace Opyn\OpynPayLater\Model\Checkout;

use Opyn\OpynPayLater\Model\Config\Settings;
use Magento\Checkout\Model\Session;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * This class in an utility method to retrieve extra data (extension configuration settings)
 */
class MoreConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{

    /**  @var Settings */
    protected Settings $settings;

    /**
     * @param Settings $settings
     */
    public function __construct(
        Settings $settings
    ) {
        $this->settings = $settings;
    }

    /**
     * This method return data to frontend in payment phase of checkout
     *
     * @return array
     */
    public function getConfig()
    {

        // This contains the extension settings variables
        $output['opyn_settings']=$this->settings->getExtParameters();

        return $output;
    }
}
